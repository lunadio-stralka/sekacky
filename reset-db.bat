@echo ! THIS WILL DELETE AND REINIT WHOLE DATABASE STRUCTURE FOR THIS PROJECT
@echo !
@echo ! PRESS ENTER TO CONTINUE OR CLOSE THIS WINDOW
pause
php ./www/clear-cache.php
php ./www/index.php orm:schema-tool:drop --force
php ./www/index.php orm:schema-tool:update --force
php ./bin/console.php instacoin:init-db
pause