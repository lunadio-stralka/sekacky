<?php

require __DIR__ . "/../app/model/Tools/FileTools.php";

\App\Model\FileTools::emptyDir(__DIR__ . "/../temp/cache");
\App\Model\FileTools::emptyDir(__DIR__ . "/../temp/proxies");
if (file_exists(__DIR__ . "/.sass-cache")) {
    \App\Model\FileTools::emptyDir(__DIR__ . "/.sass-cache");
}