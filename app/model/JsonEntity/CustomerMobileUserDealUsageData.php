<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 13:45
 */

namespace App\Model\JsonEntity;


class CustomerMobileUserDealUsageData
{
    public $cutting;
    public $cultivation;
    public $manuring;
}