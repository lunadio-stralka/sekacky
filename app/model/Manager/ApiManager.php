<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 13.08.2018
 * Time: 10:55
 */

namespace App\Model\Manager;

use App\Doctrine\Entities\CustomerMobileUserDealSettings;
use App\Doctrine\Entities\CustomerMobileUserDealUsage;
use App\Doctrine\Entities\Deal;
use App\Doctrine\Entities\User;
use Doctrine\ORM\EntityManager;

class ApiManager
{
    use \Nette\SmartObject;

    /**
     * @var EntityManager
     */
    protected $em;

    protected $testDeal; //TODO Toto v hotovem reseni vypustit

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->testDeal = $this->em->getReference(Deal::class, 1);
    }

    public function storeCutting($userId, $startTimestamp, $endTimestamp)
    {
        $cmudu = new CustomerMobileUserDealUsage();
        $cmudu->deal = $this->testDeal; //DEBUG
        $cmudu->user = $this->em->getReference(User::class, $userId);
        $cmudu->data->cutting = ['start' => $startTimestamp, 'end' => $endTimestamp];

        $this->em->persist($cmudu);
        $this->em->flush($cmudu);
    }

    public function storeCultivation($userId, $startTimestamp, $endTimestamp)
    {
        $cmudu = new CustomerMobileUserDealUsage();
        $cmudu->deal = $this->testDeal; //DEBUG
        $cmudu->user = $this->em->getReference(User::class, $userId);
        $cmudu->data->cultivation = ['start' => $startTimestamp, 'end' => $endTimestamp];

        $this->em->persist($cmudu);
        $this->em->flush($cmudu);
    }

    public function storeManuring($userId, $startTimestamp, $endTimestamp)
    {
        $cmudu = new CustomerMobileUserDealUsage();
        $cmudu->deal = $this->testDeal; //DEBUG
        $cmudu->user = $this->em->getReference(User::class, $userId);
        $cmudu->data->manuring = ['start' => $startTimestamp, 'end' => $endTimestamp];

        $this->em->persist($cmudu);
        $this->em->flush($cmudu);
    }

    public function getLastCuttingPeriod($userId)
    {
        return $this->em->getRepository(CustomerMobileUserDealUsage::class)->getLastTaskPeriod('cutting', $userId);
    }

    public function getLastCultivationPeriod($userId)
    {
        return $this->em->getRepository(CustomerMobileUserDealUsage::class)->getLastTaskPeriod('cultivation', $userId);
    }

    public function getLastManuringPeriod($userId)
    {
        return $this->em->getRepository(CustomerMobileUserDealUsage::class)->getLastTaskPeriod('manuring', $userId);
    }

    public function getAverageCuttingPeriod($userId)
    {
        return $this->em->getRepository(CustomerMobileUserDealUsage::class)->getAverageTaskPeriod('cutting', $userId);
    }

    public function getAverageCultivationPeriod($userId)
    {
        return $this->em->getRepository(CustomerMobileUserDealUsage::class)->getAverageTaskPeriod('cultivation', $userId);
    }

    public function getAverageManuringPeriod($userId)
    {
        return $this->em->getRepository(CustomerMobileUserDealUsage::class)->getAverageTaskPeriod('manuring', $userId);
    }

    public function getHoursToServis($dealId)
    {
        //TODO Je potreba zrevidovat funkci
        return $this->em->getRepository(CustomerMobileUserDealSettings::class)->getHoursToServis($dealId);
    }

    public function getManureConsumption($userId)
    {
        //TODO Je potreba doplnit funkci
        return 123;
    }
}