<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 13:13
 */

namespace App\Model\ApiEntity\ApiResponse;

class ApiResponse
{
    use \Nette\SmartObject;

    public $status = 'ok';

    public $data;

    public $error;

    public function __construct($data = null)
    {
        $this->data = $data;
    }
}