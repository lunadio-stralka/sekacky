<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 10:43
 */

namespace App\Presenters;


use App\Doctrine\Entities\CustomerMobileUserDealSettings;
use App\Doctrine\Entities\CustomerMobileUserDealUsage;
use App\Doctrine\Entities\Deal;
use App\Doctrine\Entities\User;
use App\Model\ApiEntity\ApiResponse\ApiResponse;
use App\Model\ArrayTools;
use App\Model\Manager\ApiManager;
use Doctrine\ORM\EntityManager;
use Nette\Application\Responses\JsonResponse;

class ApiPresenter extends BasePresenter
{
    /**
     * @inject
     * @var \Nette\Http\Request
     */
    public $httpRequest;

    /**
     * @inject
     * @var \Nette\Http\Response
     */
    public $httpResponse;

    /**
     * @var ApiManager
     */
    protected $apiMgr;

    protected function startup()
    {
        parent::startup();
        $this->apiMgr = new ApiManager($this->em);
    }

    public function actionStoreCutting()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !isset($postData['start']) || !isset($postData['end'])
            || !is_numeric($postData['id_user']) || !is_numeric($postData['start']) || !is_numeric($postData['end'])
        ) {
            $this->sendApiError('Parameters id_user, start and stop are required.');
        }

        $this->apiMgr->storeCutting($postData['id_user'], $postData['start'], $postData['end']);

        $response = new ApiResponse();
        $this->sendJsonResponse($response);
    }

    public function actionStoreCultivation()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !isset($postData['start']) || !isset($postData['end'])
            || !is_numeric($postData['id_user']) || !is_numeric($postData['start']) || !is_numeric($postData['end'])
        ) {
            $this->sendApiError('Parameters id_user, start and stop are required.');
        }

        $this->apiMgr->storeCultivation($postData['id_user'], $postData['start'], $postData['end']);

        $response = new ApiResponse();
        $this->sendJsonResponse($response);
    }

    public function actionStoreManuring()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !isset($postData['start']) || !isset($postData['end'])
            || !is_numeric($postData['id_user']) || !is_numeric($postData['start']) || !is_numeric($postData['end'])
        ) {
            $this->sendApiError('Parameters id_user, start and stop are required.');
        }

        $this->apiMgr->storeManuring($postData['id_user'], $postData['start'], $postData['end']);

        $response = new ApiResponse();
        $this->sendJsonResponse($response);
    }

    /**
     * Doba posledniho sekani
     */
    public function actionLastCuttingPeriod()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getLastCuttingPeriod($postData['id_user']);
        $this->sendJsonResponse($response);
    }

    /**
     * Doba posledni kultivace
     */
    public function actionLastCultivationPeriod()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getLastCultivationPeriod($postData['id_user']);
        $this->sendJsonResponse($response);
    }

    /**
     * Doba posledniho hnojeni
     */
    public function actionLastManuringPeriod()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getLastManuringPeriod($postData['id_user']);
        $this->sendJsonResponse($response);
    }

    public function actionAverageCuttingPeriod()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getAverageCuttingPeriod($postData['id_user']);
        $this->sendJsonResponse($response);
    }

    public function actionAverageCultivationPeriod()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getLastCultivationPeriod($postData['id_user']);
        $this->sendJsonResponse($response);
    }

    public function actionAverageManuringPeriod()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getLastManuringPeriod($postData['id_user']);
        $this->sendJsonResponse($response);
    }

    /**
     * TODO podle ceho hledat hodiny do servisu? Jen podle ID dealu
     */
    public function actionHoursToServis()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_deal']) || !is_numeric($postData['id_deal'])) {
            $this->sendApiError('Parameter id_deal is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getHoursToServis($postData['id_deal']);
        $this->sendJsonResponse($response);
    }

    /**
     * TODO podle ceho se pocita spotreba hnojiva? Je potreba doplnit funkci
     */
    public function actionManureConsumption()
    {
        $postData = $this->getRequestData();
        if (!isset($postData['id_user']) || !is_numeric($postData['id_user'])) {
            $this->sendApiError('Parameter id_user is required.');
        }
        $response = new ApiResponse();
        $response->data = $this->apiMgr->getManureConsumption($postData['id_user']); //DEBUG
        $this->sendJsonResponse($response);
    }

    protected function sendApiError($message)
    {
        $this->httpResponse->setCode(400);
        $response = new ApiResponse();
        $response->status = 'error';
        $response->error = $message;
        $this->sendJsonResponse($response);
    }

    protected function sendJsonResponse($response)
    {
        if (!is_array($response)) {
            $response = ArrayTools::objectToArray($response);
        }
        $response = ArrayTools::skipNullItems($response); // property, ktere jsou null se neposilaji do vystupu
        $this->sendResponse(new JsonResponse($response));
    }

    protected function getRequestData()
    {
        $postData = $this->httpRequest->rawBody;
        $array = json_decode($postData, true);
        if ($array === null || $array === false) {
            $this->sendApiError('Request has to be in JSON in POST data.');
        }
        return $array;
    }
}