<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 13:19
 */

namespace App\Presenters;


use Kdyby\Doctrine\EntityManager;

class BasePresenter extends \Nette\Application\UI\Presenter
{

    /**
     * @inject
     * @var EntityManager
     */
    public $em;

}