<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 13.08.2018
 * Time: 10:41
 */

namespace App\Presenters;


use App\Model\Manager\ApiManager;

class StatsPresenter extends BasePresenter
{
    public function renderDefault($id)
    {
        $userId = $id;
        $dealId = 1; //TODO Je potreba doplnit v hotovem reseni
        $apiMgr = new ApiManager($this->em);

        $periodFormat = 'H:i:s';

        $lastCuttingPeriod = $apiMgr->getLastCuttingPeriod($userId);
        $this->template->lastCuttingPeriod = ($lastCuttingPeriod !== null) ? (new \DateTime('@' . $lastCuttingPeriod))->format($periodFormat) : '-';

        $avgCuttingPeriod = $apiMgr->getAverageCuttingPeriod($userId);
        $this->template->avgCuttingPeriod = ($avgCuttingPeriod !== null) ? (new \Datetime('@' . $avgCuttingPeriod))->format($periodFormat) : '-';

        $lastCultivationPeriod = $apiMgr->getLastCultivationPeriod($userId);
        $this->template->lastCultivationPeriod = ($lastCultivationPeriod !== null) ? (new \DateTime('@' . $lastCultivationPeriod))->format($periodFormat) : '-';

        $avgCultivationPeriod = $apiMgr->getAverageCultivationPeriod($userId);
        $this->template->avgCultivationPeriod = ($avgCultivationPeriod !== null) ? (new \DateTime('@' . $avgCultivationPeriod))->format($periodFormat) : '-';

        $lastManuringPeriod = $apiMgr->getLastManuringPeriod($userId);
        $this->template->lastManuringPeriod = ($lastManuringPeriod !== null) ? (new \DateTime('@' . $lastManuringPeriod))->format($periodFormat) : '-';

        $avgManuringPeriod = $apiMgr->getAverageManuringPeriod($userId);
        $this->template->avgManuringPeriod = ($avgManuringPeriod !== null) ? (new \DateTime('@' . $avgManuringPeriod))->format($periodFormat) : '-';

        $manureConsumption = $apiMgr->getManureConsumption($userId);
        $this->template->manureConsumption = ($manureConsumption !== null) ? $manureConsumption . ' Kg' : '-';

        $hoursToServis = $apiMgr->getHoursToServis($dealId);
        $this->template->hoursToServis = ($hoursToServis !== null) ? $hoursToServis . ' h' : '-';
    }
}