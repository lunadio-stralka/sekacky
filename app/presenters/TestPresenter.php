<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 14:21
 */

namespace App\Presenters;


use App\Doctrine\Entities\Deal;
use App\Doctrine\Entities\User;
use Kdyby\Doctrine\EntityManager;

class TestPresenter extends BasePresenter
{
    /**
     * @inject
     * @var EntityManager
     */
    public $em;

    public function actionInit()
    {
        $user = new User();
        $deal = new Deal();

        $this->em->persist($user);
        $this->em->persist($deal);

        $this->em->flush();

        $this->terminate();
    }
}