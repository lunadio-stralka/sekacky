<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 13:40
 */

namespace App\Doctrine\Repositories;


class CustomerMobileUserDealSettingsRepo extends BaseRepository
{

    /**
     * @return string
     */
    public function alias()
    {
        return 'cmuds';
    }

    public function getHoursToServis($deal)
    {
        $cmuds = $this->findBy(['deal' => $deal]);
        if (!isset($cmuds->data->servis)) {
            return null;
        }
        return $cmuds->data->servis['hours'];
    }
}