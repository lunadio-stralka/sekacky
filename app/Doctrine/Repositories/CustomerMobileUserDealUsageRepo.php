<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 13:41
 */

namespace App\Doctrine\Repositories;


class CustomerMobileUserDealUsageRepo extends BaseRepository
{
    const ITERATION_LIMIT = 100;
    /**
     * @return string
     */
    public function alias()
    {
        return 'cmudu';
    }

    public function getLastTaskPeriod($task, $user)
    {
        for ($offset = 0; ; $offset += self::ITERATION_LIMIT) {
            $results = $this->findBy(['user' => $user], ['createDate' => 'DESC'], self::ITERATION_LIMIT, $offset);
            if (!$results) {
                return null;
            }
            foreach ($results as $result) {
                if (isset($result->data->{$task}) && $result->data->{$task} !== null) {
                    return $result->data->{$task}['end'] - $result->data->{$task}['start'];
                }
            }
        }
        return null;
    }

    public function getAverageTaskPeriod($task, $user)
    {
        $results = $this->findBy(['user' => $user], ['createDate' => 'DESC']);
        $sum = 0;
        $count = 0;
        foreach ($results as $result) {
            if (isset($result->data->{$task}) && $result->data->{$task} !== null) {
                $period = $result->data->{$task}['end'] - $result->data->{$task}['start'];
                $sum += $period;
                $count++;
            }
        }
        if (!$count) {
            return null;
        }
        return (int) round($sum / $count);
    }

}