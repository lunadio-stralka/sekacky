<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 10:17
 */

namespace App\Doctrine\Entities;

use App\Model\ArrayTools;
use App\Model\JsonEntity\CustomerMobileUserDealUsageData;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CustomerMobileUserDealUsage
 * @package App\Doctrine\Entities
 *
 * @ORM\Entity(repositoryClass="\App\Doctrine\Repositories\CustomerMobileUserDealUsageRepo")
 * @ORM\Table(name="customer_mobile_user_deal_usage")
 * @ORM\HasLifecycleCallbacks
 *
 * @property int $id
 * @property Deal $deal
 * @property array $dataJson
 * @property \DateTime $createDate
 * @property \DateTime $modifyDate
 * @property User $user
 *
 * @property CustomerMobileUserDealUsageData $data
 */
class CustomerMobileUserDealUsage extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     **/
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Deal")
     * @ORM\JoinColumn(name="id_deal",referencedColumnName="id")
     */
    protected $deal;

    /**
     * @ORM\Column(type="json_array",name="data")
     */
    protected $dataJson;

    /**
     * @ORM\Column(type="datetime",options={"default":"CURRENT_TIMESTAMP"})
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime",options={"default":"CURRENT_TIMESTAMP"})
     */
    protected $modifyDate;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="id_user",referencedColumnName="id")
     */
    protected $user;

    /**
     * Dekodovana dataJson
     * @var array
     */
    protected $_data;

    public function __construct()
    {
        $this->createDate = new \DateTime();
        $this->modifyDate = new \DateTime();
        $this->_data = new CustomerMobileUserDealUsageData();
    }

    public function getData()
    {
        return $this->_data;
    }

    protected function loadData()
    {
        $this->_data = new CustomerMobileUserDealUsageData();
        foreach ($this->dataJson as $key => $value) {
            $this->_data->{$key} = $value;
        }
    }

    /**
     * @ORM\PostLoad
     */
    public function postLoad()
    {
        $this->loadData();
    }

    /**
     * @ORM\PreFlush
     */
    public function preFlush()
    {
        $arr = ArrayTools::objectToArray($this->_data, true);
        $this->dataJson = $arr;
    }
}