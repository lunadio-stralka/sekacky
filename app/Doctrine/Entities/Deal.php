<?php
/**
 * Created by PhpStorm.
 * User: neime
 * Date: 08.08.2018
 * Time: 10:21
 */

namespace App\Doctrine\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Deal
 * @package App\Doctrine\Entities
 *
 * @ORM\Entity
 * @ORM\Table(name="deal")
 */
class Deal extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     **/
    protected $id;
}